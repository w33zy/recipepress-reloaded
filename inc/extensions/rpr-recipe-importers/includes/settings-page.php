<?php
/**
 * Setting page/modal of the Recipepress recipes importer extension
 *
 * @package Recipepress
 */

?>

<div id="<?php echo esc_attr( $this->id ); ?>" class="settings-page wrap modal slide" aria-hidden="true">
	<div class="modal__overlay" tabindex="-1" data-micromodal-close>
		<div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="settings-page-wrap-title">
			<header class="modal__header">
				<h2 id="settings-page-wrap-title" class="modal__title">
					<?php printf( '%s', esc_html( $this->title ) ); ?>
				</h2>
				<button class="modal__close" aria-label="Close modal" data-micromodal-close></button>
			</header>

			<div id="settings-page-wrap-content" class="modal__content">
				<p class="message">
                    <?php _e( 'Active 3rd party plugins with a supported importer will show up below.', 'recipepress-reloaded' ); ?>
                </p>
				<p class="warning">
                    <?php _e( 'Please ensure to make a full backup of your website. This process is only reversible via restoring a backup.', 'recipepress-reloaded' ); ?>
                </p>
                <?php
                    $screen = function_exists( 'get_current_screen' ) ? \get_current_screen() : null;

                    foreach ( $this->importer->importers as $importer ) {
                        if ( $screen && 'rpr_recipe_page_rpr_extensions' === $screen->id ) {
                            $out = '<div class="rpr recipe-importer">';
                            $out .= '<p class="rpr-import-info">';
                            $out .= esc_html( $importer->plugin_information()[$importer->action] );
                            $out .= '</p>';
                            if ( $importer->items_to_process() ) {
                                $out .= '<button class="rpr-import-button" data-recipe-importer="' . esc_attr( $importer->action ) . '">';
                                $out .= __( 'Import', 'recipepress-reloaded' );
                                $out .= '</button>';
                            }
                            $out .= '</div>';

                            echo $out; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
                        }
                    }
                ?>
			</div>

		</div>
	</div>
</div>

<style>
    .modal__container {
        width: 55%;
        max-width: 50%;
    }

    .rpr.recipe-importer {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-between;
        height: 35px;
        margin: 0 0 1rem 0;
        border: 1px solid #ccc;
        padding: 5px;
        border-radius: 3px;
        background-color: #fbf9f9;
        align-items: center;
    }

    .rpr.recipe-importer p {
        margin: 0;
    }

    button.rpr-import-button {
        background-color: green;
        color: #fff;
        border: 1px solid green;
        padding: 8px 19px;
        border-radius: 3px;
        cursor: pointer;
    }

    .modal__container p.warning {
        color: red;
        font-style: italic;
    }

    .modal__container p.message {
        font-size: 1rem;
    }
</style>

<script>
  (function($) {
      // Handles recipe importers
      const btn = $('.rpr-import-button')
      btn.on('click', function(e) {
        e.preventDefault()
        $.ajax({
            type: 'POST',
            url: rpr_script_vars.ajax_url,
            data: {
              action: 'run_recipe_importers',
              import_recipes_nonce: rpr_script_vars.rpr_import_recipes_nonce, // TODO
              import_recipe_target: $(e.target).attr('data-recipe-importer'),
            },
          },
        ).done(function() {
          // btn.hide() // TODO
        })
      })
  })(jQuery)
</script>
