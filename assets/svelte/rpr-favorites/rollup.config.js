import svelte from 'rollup-plugin-svelte'
import resolve from 'rollup-plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs'
import {terser} from 'rollup-plugin-terser'
import postcss from 'rollup-plugin-postcss'
import copy from 'rollup-plugin-copy'

const production = !process.env.ROLLUP_WATCH;

export default [
    {
        input: 'assets/src/rpr-favorites-button.js',
        output: {
            sourcemap: !production,
            format: 'iife',
            name: 'rprFavoritesButton',
            file: 'assets/dist/rpr-favorites-button.js',
        },
        plugins: [
            svelte({
                emitCss: true,
                compilerOptions: {
                    dev: !production
                }
            }),
            postcss({
                extract: 'rpr-favorites-button.css'
            }),
            copy({
                targets: [
                    {
                        src: 'assets/dist/rpr-favorites-button.js', 
                        dest: '../../../inc/extensions/rpr-favorites/assets/js'
                    },
                    {
                        src: 'assets/dist/rpr-favorites-button.js.map', 
                        dest: '../../../inc/extensions/rpr-favorites/assets/js'
                    },
                    {
                        src: 'assets/dist/rpr-favorites-button.css', 
                        dest: '../../../inc/extensions/rpr-favorites/assets/css'
                    }

                ],
                verbose: true
            }),
            resolve({
                browser: true,
                dedupe: importee => importee === 'svelte' || importee.startsWith('svelte/'),
            }),
            commonjs(),
            production && terser(),
        ],
        watch: {
            clearScreen: true,
        },
    },

    {
        input: 'assets/src/rpr-favorites-list.js',
        output: {
            sourcemap: !production,
            format: 'iife',
            name: 'rprFavoritesList',
            file: 'assets/dist/rpr-favorites-list.js',
        },
        plugins: [
            svelte({
                emitCss: true,
                compilerOptions: {
                    dev: !production
                }
            }),
            postcss({
                extract: 'rpr-favorites-list.css'
            }),
            copy({
                targets: [
                    {
                        src: 'assets/dist/rpr-favorites-list.js', 
                        dest: '../../../inc/extensions/rpr-favorites/assets/js'
                    },
                    {
                        src: 'assets/dist/rpr-favorites-list.js.map', 
                        dest: '../../../inc/extensions/rpr-favorites/assets/js'
                    },
                    {
                        src: 'assets/dist/rpr-favorites-list.css', 
                        dest: '../../../inc/extensions/rpr-favorites/assets/css'
                    }

                ],
                verbose: true
            }),
            resolve({
                browser: true,
                dedupe: importee => importee === 'svelte' || importee.startsWith('svelte/'),
            }),
            commonjs(),
            production && terser(),
        ],
        watch: {
            clearScreen: true,
        },
    },

];
