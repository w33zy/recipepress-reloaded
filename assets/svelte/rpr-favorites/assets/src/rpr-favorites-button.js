import App from './FavoritesButton/App.svelte'

let anchor = document.getElementById('rpr-favorites-button')

if ( anchor ) {
    const rprFavoritesButton = new App({
        target: anchor,
    })
}

export default rprFavoritesButton