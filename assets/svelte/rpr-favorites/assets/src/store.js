import localforage from 'localforage'
import {writable} from 'svelte/store';

export const localForage = localforage.createInstance({
    name: `${window.rpr?.rprFavoriteRecipeVars?.site_title || 'RecipepressReloaded'}`,
    storeName: 'rpr-favorite-recipes',
});
export const favoritesStore = writable([])
