import App from './FavoritesList/App.svelte'

let anchor = document.getElementById('rpr-favorites-list')

if ( anchor ) {
    const rprFavoritesList = new App({
        target: anchor,
    })
}

export default rprFavoritesList