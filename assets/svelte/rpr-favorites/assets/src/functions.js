export function shareRecipe(recipe, type) {
    if (recipe && type === 'facebook') {
        window.open(
            encodeURI(`https://www.facebook.com/sharer/sharer.php?u=${recipe.url}`),
            '',
            'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'
        )
        return false
    }

    if (recipe && type === 'twitter') {
        window.open(
            encodeURI(`https://twitter.com/intent/tweet?url=${recipe.url}&text=${recipe.title}`),
            '',
            'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'
        )
        return false
    }

    if (recipe && type === 'pinterest') {
        window.open(
            encodeURI(`https://pinterest.com/pin/create/link/?url=${recipe.url}&media=${recipe.featured_image || recipe.thumbnail}&description=${recipe.description}`),
            '',
            'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'
        )
        return false
    }

    if (recipe && type === 'email') {
        window.location.href = encodeURI(
            `mailto:?subject=${recipe.title}&body=${recipe.description}\n\nGet the Full Recipe Here: ${recipe.url}`
        )
        return false
    }
}

