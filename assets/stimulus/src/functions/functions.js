/**
 * Return an array of DOM element adjacent to the first param
 *
 * @param el The DOM element
 * @param incSelf Should the element be returned
 * @returns {[]}
 */
export function getSiblings(el, incSelf = true) {
    // for collecting siblings
    let siblings = [];

    if (!el) {
        return siblings;
    }

    // if no parent, return no sibling
    if (!el.parentNode) {
        return siblings;
    }
    // first child of the parent node
    let sibling = el.parentNode.firstChild;

    // collecting siblings
    while (sibling) {
        if (incSelf) {
            if (sibling.nodeType === 1) {
                siblings.push(sibling);
            }
        } else {
            if (sibling.nodeType === 1 && sibling !== el) {
                siblings.push(sibling);
            }
        }

        sibling = sibling.nextSibling;
    }

    return siblings;
}

/**
 *
 * @param t The current `this` context
 * @param el The element we are attaching to
 */
export function attachJQSortable(t, el) {
    jQuery(el).sortable({
        opacity: 0.6,
        revert: true,
        cursor: 'move',
        handle: '.sort-handle',
        axis: 'y',
        containment: 'parent',
        update: function () {
            t.updateIndex()
        },
    });
}

export function debounce(func, wait, immediate) {
    let timeout
    return function() {
        const context = this, args = arguments
        const later = function () {
            timeout = null
            if (!immediate) func.apply(context, args)
        };
        const callNow = immediate && !timeout
        clearTimeout(timeout)
        timeout = setTimeout(later, wait)
        if (callNow) func.apply(context, args)
    };
}

export function sanitize(input) {
    return input.replace(/<script[^>]*?>.*?<\/script>/gi, '').
    replace(/<[\/\!]*?[^<>]*?>/gi, '').
    replace(/<style[^>]*?>.*?<\/style>/gi, '').
    replace(/<![\s\S]*?--[ \t\n\r]*>/gi, '').
    trim();
}

export function makeCorsRequest(url, ingredients, callback) {
    let xhr = createCORSRequest('POST', url)

    if (!xhr) {
        console.log('CORS not supported')
        return
    }

    xhr.responseType = 'json'
    xhr.addEventListener('load', callback(xhr.response))

    // Response handlers.
    xhr.onload = function() {
        console.log(xhr.response)
        return xhr.response
    }

    xhr.onerror = function() {
        console.log('Whoops, there was an error making the request.')
    }

    xhr.setRequestHeader('Content-Type', 'application/json')
    xhr.send(JSON.stringify(ingredients))
}

export function createCORSRequest(method, url) {
    let xhr = new XMLHttpRequest()

    if ('withCredentials' in xhr) {
        xhr.open(method, url, true) // XHR for Chrome/Firefox/Opera/Safari.
    } else if (typeof XDomainRequest != 'undefined') {
        xhr = new XDomainRequest() // XDomainRequest for IE.
        xhr.open(method, url)
    } else {
        xhr = null // CORS not supported.
    }

    return xhr
}

export async function fetchData(url = '', data = {}) {
    // Default options are marked with *
    const response = await fetch(url, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
            'Content-Type': 'application/json'
            // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        body: JSON.stringify(data) // body data type must match "Content-Type" header
    })

    return response.json() // parses JSON response into native JavaScript objects
}

/**
 * Takes a string and converts it to a HTML element
 *
 * @param {string} resp The current `this` context
 *
 * @return {HTMLElement}
 */
export function parseHTML(resp) {
    const parser = new DOMParser()
    const responseDoc = parser.parseFromString(resp, 'text/html')
    let responseNode = responseDoc.body

    if (responseNode == null) {
        responseNode = document.createDocumentFragment()
    }

    return responseNode
}

/**
 * Takes a HTML element and returns a DocumentFragment containing
 * our selected CSS class
 *
 * @param {string} selector A CSS class selector
 * @param {HTMLElement} html selector A CSS class selector
 * @param {string} defaultSelector The default CSS class selector selector
 *
 * @return {DocumentFragment}
 */
export function selectFromHTML(selector, html, defaultSelector) {
    const newFragment = document.createDocumentFragment()

    if (selector) {
        html.querySelectorAll(selector).forEach(function (node) {
            newFragment.appendChild(node)
        })

        return  newFragment
    }

    if (defaultSelector) {
        html.querySelectorAll(defaultSelector).forEach(function (node) {
            newFragment.appendChild(node)
        })

        return newFragment
    }

    return newFragment
}

export function slideUp(target, duration = 500) {
    target.style.transitionProperty = 'height, margin, padding';
    target.style.transitionDuration = duration + 'ms';
    target.style.boxSizing = 'border-box';
    target.style.height = target.offsetHeight + 'px';
    target.offsetHeight;
    target.style.overflow = 'hidden';
    target.style.height = 0;
    target.style.paddingTop = 0;
    target.style.paddingBottom = 0;
    target.style.marginTop = 0;
    target.style.marginBottom = 0;

    window.setTimeout( () => {
        target.style.display = 'none';
        target.style.removeProperty('height');
        target.style.removeProperty('padding-top');
        target.style.removeProperty('padding-bottom');
        target.style.removeProperty('margin-top');
        target.style.removeProperty('margin-bottom');
        target.style.removeProperty('overflow');
        target.style.removeProperty('transition-duration');
        target.style.removeProperty('transition-property');
        //alert("!");
    }, duration);
}

export function slideDown(target, duration=500) {
    target.style.removeProperty('display');
    let display = window.getComputedStyle(target).display;

    if (display === 'none') {
        display = 'block'
    }

    target.style.display = display;
    let height = target.offsetHeight;
    target.style.overflow = 'hidden';
    target.style.height = 0;
    target.style.paddingTop = 0;
    target.style.paddingBottom = 0;
    target.style.marginTop = 0;
    target.style.marginBottom = 0;
    target.offsetHeight;
    target.style.boxSizing = 'border-box';
    target.style.transitionProperty = "height, margin, padding";
    target.style.transitionDuration = duration + 'ms';
    target.style.height = height + 'px';
    target.style.removeProperty('padding-top');
    target.style.removeProperty('padding-bottom');
    target.style.removeProperty('margin-top');
    target.style.removeProperty('margin-bottom');

    window.setTimeout( () => {
        target.style.removeProperty('height');
        target.style.removeProperty('overflow');
        target.style.removeProperty('transition-duration');
        target.style.removeProperty('transition-property');
    }, duration);
}

export function slideToggle(target, duration = 500) {
    if (window.getComputedStyle(target).display === 'none') {
        return slideDown(target, duration);
    } else {
        return slideUp(target, duration);
    }
}
