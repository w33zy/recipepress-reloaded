import { Application, defaultSchema } from 'stimulus'
import controllers from 'stimulus-controllers'

const customSchema = {
  ...defaultSchema,
  keyMappings: { ...defaultSchema.keyMappings, leftBracket: "[" },
}

const application = Application.start(document.documentElement, customSchema)
application.load(controllers)
