import {Controller} from 'stimulus'
import {parseHTML, selectFromHTML} from "../../functions/functions"

export default class extends Controller {
    selected = 'rpr-filterable-item'
    page = 2
    static targets = []

    connect() {
        window.rpr = window.rpr || {}
        rpr.rprFilterableController = this
        console.log(this)
    }

    filterItems(e) {
        e.preventDefault()

        this.selected = e.target.value
        const selects = Array.from(document.querySelectorAll('.rpr-filter-dropdown'))
        const items = Array.from(document.querySelectorAll('.rpr-filterable-item'))

        // Reset the other select drop-downs
        selects.map((select) => {
            if (select !== e.target) {
                select.value = '_'
            }
        })

        // Start by un-hiding everything
        items.map((item) => {
            item.classList.remove('rpr-hidden')
        })

        // Un-hide is we reset
        if (e.target.value === '_') {
            items.map((item) => {
                item.classList.remove('rpr-hidden')
            })

            return
        }

        // Hide everything we did not chose
        items.map((item) => {
            if (!item.classList.contains(e.target.value)) {
                item.classList.add('rpr-hidden')
            }
        })
    }

    loadMoreItems(e) {
        e.preventDefault()

        const _this = this

        document.querySelector('.rpr-filterable-load button').style.display = 'none'
        document.querySelector('.rpr-ellipsis').style.display = 'inline-block'

        jQuery.ajax({
                type: 'POST',
                url: rpr_public_vars.ajax_url,
                data: {
                    action: 'more_filtered_recipes',
                    nonce: rprFilterableVars.nonce,
                    page:  _this.page,
                    count:  rprFilterableVars.count,
                    orderby:  rprFilterableVars.orderby,
                    order:  rprFilterableVars.order,
                },
            },
        ).done(function (res) {
            if (res === '') {
                document.querySelector('.rpr-filterable-load button').style.display = 'none'
                document.querySelector('.rpr-ellipsis').style.display = 'none'
                return
            }

            const selected = _this.selected === '_' ? '' : `.${_this.selected}`
            const html = selectFromHTML(selected, parseHTML(res), '.rpr-filterable-item')

            document.getElementById('rpr-filterable-grid').append(html)

            document.querySelector('.rpr-filterable-load button').style.display = 'block'
            document.querySelector('.rpr-ellipsis').style.display = 'none'

            _this.page++
        });
    }
}