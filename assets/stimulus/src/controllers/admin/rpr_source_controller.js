import {Controller} from 'stimulus'
import {getSiblings} from "../../functions/functions";

export default class extends Controller {

    static targets = ['linkBehavior', 'linkUrl', 'linkText']

    /** Keeps track of the button that was clicked */
    button = null

    connect() {
        window.rpr = window.rpr || {}
        rpr.rprSourceController = this
    }

    /**
     * Launch our link modal to add link
     *
     * @param {MouseEvent} e The click event
     */
    addLink(e) {
        e.preventDefault()

        this.button = e.target

        // Send info on which button was pressed as an event, to launch the add link modal
        this.dispatch(
          'open',
          {
            detail: {
                button: e.target,
                linkText: this.linkTextTarget.value,
                linkUrl: this.linkUrlTarget.value,
                linkBehavior: this.linkBehaviorTarget.value,
            }
          }
        )
    }

    /**
     * Remove a saved link
     *
     * @param {MouseEvent} e The click event
     */
    removeLink(e) {
        e.preventDefault()

        this.linkTextTarget.title = ''
        this.linkUrlTarget.value = ''
        this.linkBehaviorTarget.value = ''

        getSiblings(e.target).filter(el => el.nodeName === 'BUTTON').map((elem) => {
            if (elem.classList.contains('rpr-source-add-link')) {
                elem.classList.remove('has-link')
            }
            if (elem.classList.contains('rpr-source-del-link')) {
                elem.classList.add('rpr-hidden')
            }
        })
    }

    /**
     * Get data from our CustomEvent then update
     * our inputs
     *
     * @param {CustomEvent} e The event
     */
    getData(e) {
        // If we didn't click a button in the controller, stop here
        if (! this.button) return

        const {linkText, linkUrl, linkBehavior} = e.detail
        // this.linkTextTarget.value = linkText
        this.linkUrlTarget.value = linkUrl
        this.linkBehaviorTarget.value = linkBehavior

        this.button.classList.add('has-link')
    }

}
