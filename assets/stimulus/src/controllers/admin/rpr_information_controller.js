import {Controller} from 'stimulus'

export default class extends Controller {

    static values = {
        useServingUnitsList: Boolean,
    }

    connect() {
        window.rpr = window.rpr || {}
        rpr.rprInformationController = this
    }

    fetchServingUnitsList(e) {
        if (this.useServingUnitsListValue) {
            jQuery(e.target).autocomplete({
                source: rpr.rprServingUnitsList,
                minLength: 2,
                autoFocus: true
            })
        }
    }

    destroyServingUnitsList(e) {
        if (this.useServingUnitsListValue) {
            jQuery(e.target).autocomplete('destroy')
        }
    }

}
