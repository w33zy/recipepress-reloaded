import {Controller} from 'stimulus'
import Countable from 'countable'

export default class extends Controller {

    static targets = ['wordCount']

    connect() {
        window.rpr = window.rpr || {}
        rpr.rprWordCountController = this
        this.updateCounter()

        jQuery(document).on('heartbeat-tick', (e, data) => {
            this.updateCounter()
        })
    }

    updateCounter() {
        this['wordCountTarget'].innerHTML = this.getCount()
    }

    getCount() {
        const c = []
        const recipe = {
            content:      document.querySelector('#content'),
            excerpt:      document.querySelector('#excerpt'),
            notes:        document.querySelector('#rpr_recipe_notes'),
            ingredients:  this.countIngredients(),
            instructions: this.countInstructions(),
            equipment:    this.countEquipment(),
        }

        for (let item in recipe) {
            Countable.count(
              recipe[item],
              (count) => {
                // console.log(item, count['words'])
                c.push(count['words'])
              },
              {
                  stripTags: true,
                  hardReturns: true
              }
            )
        }

        return c.reduce((a, b) => a + b, 0)
    }

    countIngredients() {
        let ingredients = []

        document.querySelectorAll('table#recipe-ingredients tr.rpr-ing-row').forEach(function (row) {
            let ingredient = []
            let amount     = row.querySelector('.ingredients_amount').value.trim()
            let unit       = row.querySelector('.ingredients_unit').value.trim()
            let name       = row.querySelector('.rpr-ing-name-input').value.trim()
            let note       = row.querySelector('.ingredients_notes').value.trim()

            ingredient.push(amount, unit, name, note)
            ingredient = ingredient.filter(item => item) // Remove empty items
            ingredients.push(ingredient.join(' ').trim())
        })

        ingredients = ingredients.filter(item => item !== '')

        if (document.getElementById('rpr-ingredients-bulk-import')) {
            ingredients.push(document.getElementById('rpr-ingredients-bulk-import').value.split("\n"))
        }

        return ingredients.filter(item => item).join(' ')
    }

    countInstructions() {
        const instructions = []

        document.querySelectorAll('table#recipe-instructions tr.rpr-ins-row').forEach(function (row) {
            let instruction = []
            let ins     = row.querySelector('.rpr-instructions__textarea').value.trim()

            instruction.push(ins);
            instruction = instruction.filter(item => item); // Remove empty items
            instructions.push(instruction.join(' ').trim());
        })

        if (document.getElementById('rpr-instruction-bulk-importer')) {
            instructions.push(document.getElementById('rpr-instruction-bulk-importer').value.split("\n"))
        }

        return instructions.filter(item => item).join(' ')
    }

    countEquipment() {
        const equipment = []

        document.querySelectorAll('table#recipe-equipment tr.rpr-equip-row').forEach(function (row) {
            let equipmentLine = []
            let name     = row.querySelector('.rpr-equip-name-input').value.trim()
            let note     = row.querySelector('.equipment_notes').value.trim()

            equipmentLine.push(name, note)
            equipmentLine = equipmentLine.filter(item => item)
            equipment.push(equipmentLine.join(' ').trim())
        });

        return equipment.filter(item => item).join(' ')
    }
}
