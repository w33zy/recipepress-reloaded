import {Controller} from 'stimulus'
import {useDebounce} from 'stimulus-use'

export default class extends Controller {

    page = 2
    search = false

    static debounces = ['searchPosts']
    static targets = ['linkUrl', 'linkText', 'linkBehaviour', 'searchField', 'linkList']


    connect() {
        window.rpr = window.rpr || {}
        rpr.rprLinkController = this

        useDebounce(this, {wait: 500})

        this.fetchLatest()
        this.linkListTarget.addEventListener('scroll', this.fetchOnScroll.bind(this))
    }

    searchSelect(e) {
        if (e.target === document.activeElement) {
            document.querySelector('.rpr-notice-default').style.display = 'none'
            document.querySelector('.rpr-notice-hint').classList.remove('screen-reader-text')
        } else {
            document.querySelector('.rpr-notice-default').style.display = 'block'
            document.querySelector('.rpr-notice-hint').classList.add('screen-reader-text')
        }
    }

    /**
     * Fetch the last 10 posts on load
     */
    fetchLatest() {
        const self = this

        jQuery.ajax({
            type: 'POST',
            url: window.ajaxurl,
            data: {
                action: 'latest_recipes_posts',
                nonce: window.rpr_script_vars.rpr_link_modal_nonce,
            },
        },
        ).done(function(res) {
            self.linkListTarget.innerHTML = res
        });
    }

    /**
     * Send an AJAX post request to get a list of
     * post based on the search field input.
     *
     * This function is debounced
     *
     * @param e The click event
     */
    searchPosts(e) {
        e.preventDefault()

        const self = this
        self.search = true
        document.querySelector('#rpr-search-panel .spinner').classList.add('busy')

        jQuery.ajax({
            type: 'POST',
            url: window.ajaxurl,
            data: {
                action: 'search_recipes_posts',
                search: self.searchFieldTarget.value,
                nonce: window.rpr_script_vars.rpr_link_modal_nonce,
            },
        },
        ).done(function(res) {
            self.linkListTarget.innerHTML = res
            document.querySelector('#rpr-search-panel .spinner').classList.remove('busy')
        });
    }

    fetchOnScroll() {
        const self = this
        const el = document.getElementById('rpr-post-list')

        // If we are searching don't fetch more results on scroll
        if (self.search) {
            return
        }

        if (el.scrollHeight - el.scrollTop - el.clientHeight < 1) {
            jQuery.ajax({
                    type: 'POST',
                    url: window.ajaxurl,
                    data: {
                        action: 'latest_recipes_posts',
                        nonce: window.rpr_script_vars.rpr_link_modal_nonce,
                        page: self.page,
                    },
                },
            ).done(function (res) {
                document.getElementById('rpr-post-list').insertAdjacentHTML('beforeend', res)
                self.page++
            });
        }
    }

    /**
     * When a item is selected from the posts list,
     * add its corresponding values to the input fields
     *
     * @param e The click event
     */
    selectListItem(e) {
        e.preventDefault()
        this.linkUrlTarget.value = e.currentTarget.getAttribute('data-permalink')
        this.linkTextTarget.value = e.currentTarget.getAttribute('data-title')
    }

    /**
     * When the "Add Link" button clicked
     * create a custom event with the selected details
     *
     * @param e The click event
     */
    addLink(e) {
        this.dispatch(
          'data',
          {
            detail: {
              linkUrl: this.linkUrlTarget.value,
              linkText: this.linkTextTarget.value,
              linkBehavior: this.linkBehaviourTarget.checked ? 'new' : 'same',
            }
          }
        )

        this.closeModal(e)
    }

    /**
     * Close the modal and set the inputs to empty values
     *
     * @param e The click event
     */
    openModal(e) {
        e && e.preventDefault()

        //document.getElementById('rpr-link-modal-backdrop').style.display = 'block' // TODO: Fix this
        this.element.classList.add('rpr-modal-show')
    }

    /**
     * Close the modal and set the inputs to empty values
     *
     * @param e The click event
     */
    closeModal(e) {
        e && e.preventDefault()

        this.search = false

        this.linkUrlTarget.value = ''
        this.linkTextTarget.value = ''
        this.searchFieldTarget.value = ''
        this.linkBehaviourTarget.checked = false

        //document.getElementById('rpr-link-modal-backdrop').style.display = 'none' // TODO: Fix this
        this.element.classList.remove('rpr-modal-show')
    }

    /**
     * Receives a CustomEvent with link details from
     * the add link button
     *
     * This is then use to pre-populate the modal with previous
     * values if present
     *
     * @param {CustomEvent} e The click event
     */
    getLinkData(e) {
        this.openModal()

        const {linkText, linkUrl, linkBehavior} = e.detail

        this.linkTextTarget.value = linkText
        this.linkUrlTarget.value = linkUrl
        this.linkBehaviourTarget.checked = linkBehavior === 'new'
    }

}
