import { Controller } from 'stimulus'
import {getSiblings} from "../../functions/functions";

export default class extends Controller {

    static targets = []

    connect() {
        window.rpr = window.rpr || {}
        rpr.rprSettingsController = this
    }

    /**
     * The `Advanced` tab
     */

    unmaskInputFields(e) {
        e.preventDefault()
        const input = e.target.previousSibling
        const siblings = getSiblings(e.target)

        if (input.getAttribute('type') === 'password') {
            input.setAttribute('type', 'text')
            siblings.map(el => {
                if (el.nodeName === 'BUTTON') {
                    el.querySelector('.eye').style.display = 'none'
                    el.querySelector('.eye-blocked').style.display = 'block'
                }
            })
            return
        }

        if (input.getAttribute('type') === 'text') {
            input.setAttribute('type', 'password')
            siblings.map(el => {
                if (el.nodeName === 'BUTTON') {
                    el.querySelector('.eye').style.display = 'block'
                    el.querySelector('.eye-blocked').style.display = 'none'
                }
            })
        }
    }
}