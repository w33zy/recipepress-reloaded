import {Controller} from 'stimulus'
import {fetchData} from "../../functions/functions";

export default class extends Controller {

    static targets = ['calories', 'joules', 'servings', 'protein', 'fat', 'carbohydrate']
    static values = {
        edamamKeys: Object,
    }
    recipe; ingredients; recipeCount; recipeServings; recipeYield; recipeTitle;

    connect() {
        window.rpr = window.rpr || {}
        rpr.rprNutritionController = this
    }

    calculateCalories(e) {
        e.preventDefault()

        if (e.target.id === 'rpr_recipe_calorific_value') {
            this.joulesTarget.value = Math.round(this.caloriesTarget.value * 4.18)
        }
        if (e.target.id === 'rpr_recipe_calorific_value_kj') {
            this.caloriesTarget.value = Math.round(this.joulesTarget.value / 4.18)
        }
    }

    getIngredientsData() {

        this.ingredients = []
        this.recipeCount = document.getElementById('rpr_recipe_servings').value.trim() || 1
        this.recipeServings = document.getElementById('rpr_recipe_servings_type').value.trim() || 'serving'
        this.recipeYield = `${this.recipeCount} ${this.recipeServings}`
        this.recipeTitle = document.getElementById('title').value.trim()

        const ingRows = Array.from(document.querySelectorAll('#recipe-ingredients tr.rpr-ing-row'))
                        .filter(el => el.classList.contains('rpr-ing-row'))

        for (let i = 0; i < ingRows.length; i++) {
            let row = ingRows[i]
            let ingredient = [], amount, unit, name, note

            row.querySelectorAll('input').forEach((item) => {
                if (item.classList.contains('ingredients_amount')) amount = item.value.trim()
                if (item.classList.contains('ingredients_unit')) unit = item.value.trim()
                if (item.classList.contains('rpr-ing-name-input')) name = item.value.trim()
                if (item.classList.contains('ingredients_notes')) note = item.value.trim()
            })

            ingredient.push(amount, unit, name);
            ingredient = ingredient.filter(item => item); // Remove empty items
            this.ingredients.push(ingredient.join(' ').trim());
        }

        this.recipe = {
            title: this.recipeTitle,
            ingr: this.ingredients.filter(item => item), // Remove empty items
            yield: this.recipeYield
        };

        return this.recipe
    }

    fetchNutrition(e) {
        e.preventDefault()

        const url = `https://api.edamam.com/api/nutrition-details?app_id=${this.edamamKeysValue.appId}&app_key=${this.edamamKeysValue.appKey}`

        fetchData(url, this.getIngredientsData())
        .then((data) => {
            if (data.hasOwnProperty('calories')) {
                this.caloriesTarget.value = Math.round(data.calories / (parseInt(this.recipeCount) || 1))
                this.joulesTarget.value = Math.round(this.caloriesTarget.value * 4.18)
            }
            if (data.hasOwnProperty('totalNutrients')) {
                this.proteinTarget.value = Math.round(data.totalNutrients.PROCNT.quantity / (parseInt(this.recipeCount) || 1))
                this.fatTarget.value = Math.round(data.totalNutrients.FAT.quantity / (parseInt(this.recipeCount) || 1))
                this.carbohydrateTarget.value = Math.round(data.totalNutrients.CHOCDF.quantity / (parseInt(this.recipeCount) || 1))
            }
            this.servingsTarget.value = 'per_serving'
        })
        .catch((error) => {
            console.error(error)
        })
    }

    expandNutrition(e) {
        e.preventDefault()

        const icon = document.querySelector('.expand-indicator')

        jQuery('.additional_nutrition')
          .slideToggle(
            400,
            function() {
                if (icon.classList.contains('dashicons-arrow-down-alt2')) {
                    icon.classList.remove('dashicons-arrow-down-alt2')
                    icon.classList.add('dashicons-arrow-up-alt2')
                } else {
                    icon.classList.remove('dashicons-arrow-up-alt2')
                    icon.classList.add('dashicons-arrow-down-alt2')
                }
            }
        )
    }



}