import { Controller } from 'stimulus'
import { sanitize } from '../../functions/functions'

export default class extends Controller {

    static targets = ['videoURL', 'videoTitle', 'videoDescription', 'videoDate',
                        'videoThumbnail0', 'videoThumbnail1', 'videoThumbnail2', 'videoContainer',
                        'fetchButton', 'removeButton']

    connect() {
        window.rpr = window.rpr || {}
        rpr.rprVideoController = this
    }

    /**
     * Fetches the video data from YouTube
     *
     * @param {MouseEvent} e The click event
     */
    fetchVideo(e) {
        e.preventDefault()
        const _this = this

        const videoURL = this.videoURLTarget.value
        let YoutubeID = videoURL.match(
            /(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/,
        );
        let vimeoID = videoURL.match(
            /(http|https)?:\/\/(www\.|player\.)?vimeo\.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|video\/|)(\d+)(?:|\/\?)/,
        );

        let videoService
        let videoID = YoutubeID || vimeoID

        if (videoID) {
            if (YoutubeID) {
                videoID = videoID[1];
                videoService = 'YouTube'
            }
            if (vimeoID) {
                videoID = videoID[4];
                videoService = 'Vimeo'
            }
        } else {
            alert('Invalid video URL provided. For now, this feature only supports YouTube and Vimeo videos.');
            return false;
        }

        jQuery.ajax({
            url: window.rpr_script_vars.ajax_url,
            type: 'POST',
            data: {
                action: 'fetch_video_data',
                video_id: videoID,
                video_service: videoService,
                rpr_video_nonce: window.rpr_script_vars.rpr_video_nonce,
            },
        }).done(function(resp) {
            if (resp.data.hasOwnProperty('error')) {
                if (videoService === 'Vimeo') _this.videoContainerTarget.innerHTML = `<p>${resp.data.error}</p>`
                if (videoService === 'YouTube') _this.videoContainerTarget.innerHTML = `<p>${resp.data.error.message}</p>`
                return
            }

            if ( videoService === 'YouTube' && resp.data.items.length === 0) {
                _this.videoContainerTarget.innerHTML = `<p>No videos found</p>`
                return
            }

            if (videoService === 'YouTube') {
                const video = resp.data.items[0]
                console.log(video)
                _this.videoTitleTarget.value = sanitize(video.snippet.title)
                _this.videoDescriptionTarget.value = sanitize(video.snippet.description)
                _this.videoDateTarget.value = sanitize(video.snippet.publishedAt)
                _this.videoThumbnail0Target.value = sanitize(video.snippet.thumbnails.default.url)
                _this.videoThumbnail1Target.value = sanitize(video.snippet.thumbnails.high.url)
                _this.videoThumbnail2Target.value = sanitize(video.snippet.thumbnails.medium.url)

                _this.videoContainerTarget.innerHTML = `<img src="${sanitize(video.snippet.thumbnails.high.url)}" 
                                                        alt="${sanitize(video.snippet.description)}"/>`
            } else if (videoService === 'Vimeo') {
                const video = resp.data
                console.log(video)
                _this.videoTitleTarget.value = sanitize(video.name)
                _this.videoDescriptionTarget.value = sanitize(video.description)
                _this.videoDateTarget.value = sanitize(video.created_time)
                _this.videoThumbnail0Target.value = sanitize(video.pictures.sizes[3].link)
                _this.videoThumbnail1Target.value = sanitize(video.pictures.sizes[6].link)
                _this.videoThumbnail2Target.value = sanitize(video.pictures.sizes[9].link)

                _this.videoContainerTarget.innerHTML = `<img src="${sanitize(video.pictures.sizes[6].link)}" 
                                                        alt="${sanitize(video.name)}"/>`
            } else {
                return
            }

            _this.fetchButtonTarget.classList.add('rpr-hidden')
            _this.removeButtonTarget.classList.remove('rpr-hidden')

        }).fail(function(data) {
            alert(data.responseJSON.error.message);
        });
    }

    /**
     * Remove the saved video data
     *
     * @param {MouseEvent} e The click event
     */
    removeVideo(e) {
        e.preventDefault()

        this.videoURLTarget.value = ''
        this.videoTitleTarget.value = ''
        this.videoDescriptionTarget.value = ''
        this.videoDateTarget.value = ''
        this.videoThumbnail0Target.value = ''
        this.videoThumbnail1Target.value = ''
        this.videoThumbnail2Target.value = ''
        this.videoContainerTarget.innerHTML = ''

        this.fetchButtonTarget.classList.remove('rpr-hidden')
        this.removeButtonTarget.classList.add('rpr-hidden')
    }

    /**
     * Opens the video in a new tab
     */
    goToVideo() {
        const win = window.open(this.videoURLTarget.value, '_blank');
        win.focus();
    }

}
