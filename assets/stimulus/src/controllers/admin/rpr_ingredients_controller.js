import { Controller } from 'stimulus'
import { getSiblings, attachJQSortable } from "../../functions/functions"
import autosize from 'autosize/dist/autosize'

export default class extends Controller {

    button = null
    static targets = [
        'bulkIngredientsImportTextarea',
        'addIngredientRowButton',
        'addIngredientGroupButton',
        'bulkIngredientImportButton'
    ]

    static values = {
        ingredientsImporter: Boolean,
        unitList: Boolean,
    }

    initialize() {
        this.switchToBulkImporter()
    }

    connect() {
        window.rpr = window.rpr || {}
        rpr.rprIngredientsController = this

        attachJQSortable(this, '#recipe-ingredients tbody')

        if (this.hasBulkIngredientsImportTextareaTarget) {
            autosize(this.bulkIngredientsImportTextareaTarget)
        }
    }

    /**
     * Create an autocomplete on the `focusin` event
     *
     * @param {FocusEvent} e
     */
    fetchIngredients(e) {
        jQuery(e.target).autocomplete({
            source: rpr.rprIngredientList,
            minLength: 2,
            autoFocus: true
        })
    }

    fetchUnitsList(e) {
        if (!this.unitListValue) {
            return
        }
        jQuery(e.target).autocomplete({
            source: rpr.rprIngredientsUnitList,
            minLength: 2,
            autoFocus: true
        })
    }

    /**
     * Destroy our autocomplete on the `focusout` event
     *
     * @param {FocusEvent} e
     */
    destroyFetchIngredients(e) {
        jQuery(e.target).autocomplete('destroy')
    }

    destroyUnitsList(e) {
        if (!this.unitListValue) {
            return
        }
        jQuery(e.target).autocomplete('destroy')
    }

    addHeading(e) {
        e.preventDefault()

        let cloneFrom = document.querySelector('#recipe-ingredients tr.ingredient-group-stub')
        let lastRow = document.querySelector('#recipe-ingredients tbody tr:last-child')

        // If the button is the inline button then the last row is the current row
        if (e.target.classList.contains('rpr-ing-add-heading-inline') || e.code === 'Tab') {
            lastRow = e.target.parentElement.parentElement
        }

        let clonedHeading = cloneFrom.cloneNode(true)

        clonedHeading.classList.add('ingredient-group', 'row-0')
        clonedHeading.classList.remove('ingredient-group-stub', 'rpr-hidden')
        clonedHeading.querySelector('input').value = ''

        /*if (e.target.classList.contains('rpr-ing-add-heading')) {
            lastRow = e.target.parentElement.parentElement
        }*/

        // Insert the clone row after the row that clicked the button
        lastRow.parentNode.insertBefore(clonedHeading, lastRow.nextSibling)

        clonedHeading.querySelector('.ingredient-group-input').focus()

        this.updateIndex()
    }

    removeHeading(e) {
        e.preventDefault()

        const rows = getSiblings(e.target.parentElement.parentElement)
            .filter(el => el.classList.contains('ingredient-group'))

        const row = e.target.parentElement.parentElement
        const newRows = []
        let removedIndex = 0

        for (const [i, el] of rows.entries()) {
            if (row === el) {
                removedIndex = i
                el.remove()
            } else {
                newRows.push(el)
            }
        }

        if (newRows[removedIndex]) {
            newRows[removedIndex].querySelector('.ingredient-group-input').focus()
        }

        this.updateIndex()
    }

    addIngredient(e) {
        e.preventDefault()

        // Get the last for that is empty of has (saved) information
        let lastRow = document.querySelector('#recipe-ingredients tbody tr:last-child')
        //let lastIng = document.querySelector('#recipe-ingredients tr.rpr-ing-row:last-child')
        let lastIng = Array.from((document.querySelectorAll('#recipe-ingredients tr.rpr-ing-row'))).pop()

        // If a button was press grab the entire row it was pressed on
        if (e.target.classList.contains('rpr-ing-add-row-inline') || e.code === 'Tab') {
            lastRow   = e.target.parentElement.parentElement
            lastIng = lastRow
        }

        // Clone the row and its information
        let clonedIng = lastIng.cloneNode(true)

        // Set the placeholder and values on our inputs to be empty
        clonedIng.querySelectorAll('input').forEach(function (item) {
            item.value = ''
        })

        // Hide the delete button on clone
        const addLinkBtn = clonedIng.querySelector('.rpr-ing-add-link')
        if (addLinkBtn) {
            addLinkBtn.classList.remove('has-link')
            addLinkBtn.title = ''
        }

        const removeLinkBtn = clonedIng.querySelector('.rpr-ing-del-link')
        if (removeLinkBtn) {
            removeLinkBtn.classList.add('rpr-hidden')
        }

        // If the
        if (e.target.classList.contains('rpr-ing-add-row-bottom')) {
            lastIng = lastRow
        }

        // Insert the clone row after the row that clicked the button
        lastIng.parentNode.insertBefore(clonedIng, lastIng.nextSibling)

        // Set the focus to the 'amount' input field
        clonedIng.querySelector('.ingredients_amount').focus()

        // Reindex our rows
        this.updateIndex()
    }

    removeIngredient(e) {
        e.preventDefault()

        const rows = getSiblings(e.target.parentElement.parentElement)
                    .filter(el => el.classList.contains('rpr-ing-row'))
        const row = e.target.parentElement.parentElement
        const newRows = []
        let removedIndex = 0

        for (const [i, el] of rows.entries()) {
            if (el.classList.contains(row.className.split(' ').pop()) && row === el) {
                removedIndex = i
                if (rows.length < 2) return // Prevent removing all rows
                el.remove()
            } else {
                newRows.push(el)
            }
        }

        if (newRows[removedIndex]) {
            newRows[removedIndex].querySelector('.ingredients_amount').focus()
        }

        this.updateIndex()
    }

    /**
     * Duplicate the row if we are in the "notes"
     * or the "heading" input and press the `Tab` key
     *
     * @param {KeyboardEvent} e The click event
     */
    tabToDuplicate(e) {
        if (e.code === 'Tab' || e.key === 'Tab') {
            if (e.target.classList.contains('ingredient-group-input')) {
                this.addHeading(e)
            }
            if (e.target.classList.contains('ingredients_notes')) {
                this.addIngredient(e)
            }
        }
    }

    addLink(e) {
        e.preventDefault()

        const data = {}
        this.button = e.target

        getSiblings(e.target).map((el) => {
            if (el.classList.contains('rpr_recipe_ingredients_link')) {
                data.linkUrl = el.value
            }
            if (el.classList.contains('rpr_recipe_ingredients_target')) {
                data.linkBehavior = el.value
            }
            data.linkText = ''
        })

        // Send info on which button was pressed as an event, to launch the add link modal
        this.dispatch('open', {detail: {button: e.target, ...data }})
    }

    removeLink(e) {
        e.preventDefault()

        getSiblings(e.target).map((el) => {
            if (el.classList.contains('rpr_recipe_ingredients_link')) {
                el.value = ''
            }
            if (el.classList.contains('rpr_recipe_ingredients_target')) {
                el.value = ''
            }
            if (el.classList.contains('rpr-ing-del-link')) {
                el.classList.add('rpr-hidden')
            }
            if (el.classList.contains('rpr-ing-add-link')) {
                el.classList.remove('has-link')
                el.title = ''
            }
        })
    }

    /**
     * Receives a CustomEvent with link details from
     * the link modal
     *
     * @param {CustomEvent} e The click event
     */
    getLinkData(e) {
        // If we didn't click a button in the controller, stop here
        if (!this.button) return

        const {linkUrl, linkBehavior} = e.detail

        getSiblings(this.button).map((el) => {
            if (el.classList.contains('rpr_recipe_ingredients_link')) {
                el.value = linkUrl
            }
            if (el.classList.contains('rpr_recipe_ingredients_target')) {
                el.value = linkBehavior
            }
            if (el.classList.contains('rpr-ing-del-link')) {
                el.classList.remove('rpr-hidden')
            }
            if (el.classList.contains('rpr-ing-add-link')) {
                el.classList.add('has-link')
                el.title = linkUrl
            }
        })
    }

    updateIndex() {
        const rows = Array.from(document.querySelectorAll('#recipe-ingredients tbody tr'))
            .filter(el => el.classList.contains('rpr-ing-row') || el.classList.contains('ingredient-group'))

        for (let i = 0; i < rows.length; i++) {
            let item = rows[i]

            // Update the `class` attribute
            item.classList.remove(item.className.split(' ').pop())
            item.classList.add('row-' + (i + 1))

            item.querySelectorAll('input').forEach((input) => {
                if (input.classList.contains('ingredients_sort')) {
                    input.value = i+1 // Set the value of the `value` attribute
                }

                let name = input.attributes['name'].value
                name = name.replace(/\[(\d+)\]/, '[' + (i + 1) + ']')

                let id = input.attributes['id'].value
                id = id.replace(/\_(\d+)/, '_' + (i + 1))

                // Update the `name` attribute of the input
                input.setAttribute('name', name)

                // Update the `id` attribute of the input
                input.setAttribute('id', id)
            })
        }
    }
    switchToBulkImporter(e = null) {
        e && e.preventDefault()

        this.ingredientsImporterValue = ! this.ingredientsImporterValue

        // Find the .rpr-ing-row class and hide it
        document.querySelectorAll('.rpr-ing-row').forEach((el) => {
            el.hidden = ! this.ingredientsImporterValue
        })

        // Find the .ingredient-group class and hide it
        document.querySelectorAll('.ingredient-group').forEach((el) => {
            el.hidden = ! this.ingredientsImporterValue
        })

        // Find the .rpr-ing-bulk-import class and show it
        document.querySelectorAll('.rpr-ing-bulk-import').forEach((el) => {
            el.hidden = this.ingredientsImporterValue
        })

        // Hide the "Add an ingredient" and "Add a group" buttons
        if (this.hasAddIngredientRowButtonTarget) {
            if (! this.ingredientsImporterValue) {
                this.addIngredientRowButtonTarget.style.display = 'none'
            } else {
                this.addIngredientRowButtonTarget.style.display = 'inline-flex'
            }
        }
        if (this.hasAddIngredientGroupButtonTarget) {
            if (! this.ingredientsImporterValue) {
                this.addIngredientGroupButtonTarget.style.display = 'none'
            } else {
                this.addIngredientGroupButtonTarget.style.display = 'inline-flex'
            }
        }
    }

    stripUnwantedCharacters(e) {
        e.preventDefault()

        let text = e.clipboardData.getData('text')

        // Strip bullet characters and 1. 2) etc.
        text = text.replace(/[\u2022\u00B7\u00A7\u25CF]/g, '')
        text = text.replace(/(\d+)(\.|\))(\t)/g, '')

        // Get the current cursor position
        const selectionStart = e.target.selectionStart
        const selectionEnd = e.target.selectionEnd

        // Insert the stripped text at the cursor position
        const currentValue = e.target.value
        const newValue = currentValue.substring(0, selectionStart) + text + currentValue.substring(selectionEnd)
        e.target.value = newValue

        // Move the cursor to the end of the inserted text
        const newCursorPosition = selectionStart + text.length
        e.target.setSelectionRange(newCursorPosition, newCursorPosition)

        autosize.update(e.target)
    }

    wrapIngredientSelection(e) {
        const selectionStart = e.target.selectionStart
        const selectionEnd = e.target.selectionEnd
        const selectedText = e.target.value.substring(selectionStart, selectionEnd)

        let wrappedText = `[`

        if (selectedText.length > 0) {
            wrappedText = `[${selectedText}]`
        }

        // Replace the selected text with the wrapped text
        const textBeforeSelection = e.target.value.substring(0, e.target.selectionStart)
        const textAfterSelection = e.target.value.substring(e.target.selectionEnd)
        e.target.value = textBeforeSelection + wrappedText + textAfterSelection

        // Set the cursor position after the wrapped text
        const newCursorPosition = selectionStart + wrappedText.length
        e.target.setSelectionRange(newCursorPosition, newCursorPosition)

        e.preventDefault()
    }

    /**
     *
     * @param e {ClipboardEvent}
     * @returns {void}
     */
    // insertURL(e) {
    //     e.preventDefault()
    //     console.log('Inserted URL', e)
    //
    //     let newText
    //     const selectionStart = e.target.selectionStart
    //     const selectionEnd = e.target.selectionEnd
    //     const selectedText = e.target.value.substring(selectionStart, selectionEnd)
    //     console.log(selectedText)
    //     const clipboardText = e.clipboardData.getData('text')
    //     const urlRegex = /(ftp|http|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?/gi;
    //
    //     const isUrl = urlRegex.test(clipboardText)
    //
    //     if (selectedText.length > 0 && isUrl ) {
    //         newText = `[${selectedText}](${clipboardText})`
    //     }
    //
    //     console.log(newText)
    //
    //     const textBeforeSelection = e.target.value.substring(0, e.target.selectionStart)
    //     const textAfterSelection = e.target.value.substring(e.target.selectionEnd)
    //     e.target.value = textBeforeSelection + (newText || selectedText) + textAfterSelection
    //
    //     const newCursorPosition = newText ? (selectionStart + newText.length) : (selectionStart + selectedText.length)
    //     e.target.setSelectionRange(newCursorPosition, newCursorPosition)
    // }

    // insertURL(e) {
    //     e.preventDefault();
    //     //console.log('Inserted URL', e);
    //
    //     const selectedText = this.getSelectedText(e.target);
    //     //console.log(selectedText);
    //
    //     const clipboardText = this.getClipboardText(e);
    //     const isUrl = this.validateURL(clipboardText);
    //
    //     if (selectedText && isUrl) {
    //         const modifiedText = this.createModifiedText(selectedText, clipboardText);
    //         console.log(modifiedText);
    //         this.replaceTextInRange(e.target, modifiedText);
    //         this.setCursorPosition(e.target, modifiedText.length);
    //     }
    // }
    //
    // getSelectedText(input) {
    //     return input.value.substring(input.selectionStart, input.selectionEnd);
    // }
    //
    // getClipboardText(e) {
    //     return (e.clipboardData && e.clipboardData.getData('text')) || '';
    // }
    //
    // validateURL(url) {
    //     const urlRegex = /(ftp|http|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?/gi;
    //     return urlRegex.test(url);
    // }
    //
    // createModifiedText(selectedText, clipboardText) {
    //     return `[${selectedText}](${clipboardText})`;
    // }
    //
    // replaceTextInRange(input, newText) {
    //     const textBeforeSelection = input.value.substring(0, input.selectionStart);
    //     const textAfterSelection = input.value.substring(input.selectionEnd);
    //     input.value = textBeforeSelection + newText + textAfterSelection;
    // }
    //
    // setCursorPosition(input, position) {
    //     input.setSelectionRange(position, position);
    // }

}
