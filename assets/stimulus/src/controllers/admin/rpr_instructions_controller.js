import {Controller} from 'stimulus'
import {getSiblings, attachJQSortable} from "../../functions/functions";
import autosize from 'autosize/dist/autosize'

export default class extends Controller {

    static targets = [
        'bulkInstructionsImportTextarea',
        'addInstructionRowButton',
        'addInstructionGroupButton',
        'bulkInstructionImportButton'
    ]

    static values = {
        instructionsImporter: Boolean,
    }

    initialize() {
        this.switchToBulkImporter()
    }

    connect() {
        window.rpr = window.rpr || {}
        rpr.rprInstructionsController = this
        attachJQSortable(this, '#recipe-instructions tbody')

        if (this.hasBulkInstructionsImportTextareaTarget) {
            autosize(this.bulkInstructionsImportTextareaTarget)
        }
    }

    addHeading(e) {
        e.preventDefault()

        let cloneFrom = document.querySelector('#recipe-instructions tr.instruction-group-stub')
        let lastRow = document.querySelector('#recipe-instructions tbody tr:last-child')

        // If the button is the inline button then the last row is the current row
        if (e.target.classList.contains('rpr-ins-add-heading-inline') || e.code === 'Tab') {
            lastRow = e.target.parentElement.parentElement
        }

        let clonedHeading = cloneFrom.cloneNode(true)

        clonedHeading.classList.add('instruction-group', 'row-0')
        clonedHeading.classList.remove('instruction-group-stub', 'rpr-hidden')
        clonedHeading.querySelector('input').value = ''

        // Insert the clone row after the row that clicked the button
        lastRow.parentNode.insertBefore(clonedHeading, lastRow.nextSibling)

        clonedHeading.querySelector('.instructions-group-label').focus()

        this.updateIndex()
    }

    removeHeading(e) {
        e.preventDefault()

        const rows = getSiblings(e.target.parentElement.parentElement)
            .filter(el => el.classList.contains('instruction-group'))

        const row = e.target.parentElement.parentElement
        const newRows = []
        let removedIndex = 0

        for (const [i, el] of rows.entries()) {
            if ( row === el) {
                removedIndex = i
                el.remove()
            } else {
                newRows.push(el)
            }
        }

        if (newRows[removedIndex]) {
            newRows[removedIndex].querySelector('.instructions-group-label').focus()
        }

        this.updateIndex()
    }

    addInstruction(e) {
        e.preventDefault()

        let lastRow = document.querySelector('#recipe-instructions tbody tr:last-child')
        let lastIns = Array.from((document.querySelectorAll('#recipe-instructions tr.rpr-ins-row'))).pop()

        if (e.target.classList.contains('rpr-ins-add-row-inline') || e.code === 'Tab') {
            lastRow = e.target.parentElement.parentElement
            lastIns = lastRow
        }

        // Clone the row and its information
        let clonedIns = lastIns.cloneNode(true)

        // Empty out the values of the cloned row
        clonedIns.querySelector('.rpr-ins-instruction textarea').value = ''
        clonedIns.querySelector('.rpr_recipe_instructions_thumbnail').setAttribute('src', '')
        clonedIns.querySelector('.rpr_recipe_instructions_thumbnail').style.display = 'none'
        clonedIns.querySelector('.rpr-ins-image-del').style.display = 'none'
        clonedIns.querySelector('.rpr-ins-image-set').style.display = 'block'

        // Set the placeholder and values on our inputs to be empty
        clonedIns.querySelectorAll('input').forEach(function (item) {
            item.value = ''
        })

        if (e.target.classList.contains('rpr-ins-add-row-bottom')) {
            lastIns = lastRow
        }

        // Insert the clone row after the row that clicked the button
        lastIns.parentNode.insertBefore(clonedIns, lastIns.nextSibling)

        // Set the focus to the 'textarea' input field
        clonedIns.querySelector('.rpr-ins-instruction textarea').focus()

        this.updateIndex()
    }

    removeInstruction(e) {
        e.preventDefault()

        const rows = getSiblings(e.target.parentElement.parentElement)
            .filter(el => el.classList.contains('rpr-ins-row'))
        const row = e.target.parentElement.parentElement
        const newRows = []
        let removedIndex = 0

        for (const [i, el] of rows.entries()) {
            if ( row === el) {
                removedIndex = i
                if (rows.length < 2) return // Prevent removing all rows
                el.remove()
            } else {
                newRows.push(el)
            }
        }

        if (newRows[removedIndex]) {
            newRows[removedIndex].querySelector('.rpr-ins-instruction textarea').focus()
        }

        this.updateIndex()
    }

    /**
     * Duplicate the row if we are in the "notes"
     * or the "heading" input and press the `Tab` key
     *
     * @param {KeyboardEvent} e The keyboard event
     */
    tabToDuplicate(e) {
        if (e.code === 'Tab' || e.key === 'Tab') {
            if (e.target.classList.contains('rpr-instructions__input-heading')) {
                this.addHeading(e)
            }
            if (e.target.classList.contains('rpr-instructions__textarea')) {
                this.addInstruction(e)
            }
        }
    }

    updateIndex() {
        const rows = Array.from(document.querySelectorAll('#recipe-instructions tbody tr'))
            .filter(el => el.classList.contains('rpr-ins-row') || el.classList.contains('instruction-group'))

        for (let i = 0; i < rows.length; i++) {
            let item = rows[i]

            // Update the `class` attribute
            item.classList.remove(item.className.split(' ').pop())
            item.classList.add('row-' + (i + 1))

            item.querySelectorAll('input, select, textarea').forEach((input) => {

                if (input.classList.contains('instructions_sort')) {
                    input.value = i+1 // Set the value of the `value` attribute
                }

                let name = input.attributes['name'].value
                name = name.replace(/\[(\d+)\]/, '[' + (i + 1) + ']')

                let id = input.attributes['id'].value
                id = id.replace(/\_(\d+)/, '_' + (i + 1))

                // Update the `name` attribute of the input
                input.setAttribute('name', name)

                // Update the `id` attribute of the input
                input.setAttribute('id', id)
            })
        }

    }
    
    addImage(e) {
        e.preventDefault()

        let imageID
        let imageSrc
        let addBtn
        let delBtn

        getSiblings(e.target).map((el) => {
            if (el.classList.contains('rpr-ins-image-del')) {
                delBtn = el
            }
            if (el.classList.contains('rpr-ins-image-set')) {
                addBtn = el
            }
            if (el.classList.contains('rpr_recipe_instructions_thumbnail')) {
                imageSrc = el
            }
            if (el.classList.contains('rpr_recipe_instructions_image')) {
                imageID = el
            }
        })

        if (wp.media && typeof wp.media === 'function') {
            const customUploader = wp.media({
                title: 'Add instruction photo', //ins_trnsl.ins_img_upload_title,
                button: {
                    text: 'Add photo',//ins_trnsl.ins_img_upload_text
                },
                multiple: false,
                frame: 'select'
            })
            customUploader.on('select', function() {
                const attachment = customUploader.state().get('selection').first().toJSON();
                console.log(attachment)

                if (imageSrc && attachment.sizes.hasOwnProperty('medium')) {
                    imageSrc.setAttribute('src', attachment.sizes.medium.url)
                } else if (imageSrc) {
                    imageSrc.setAttribute('src', attachment.url)
                }

                imageID.value = attachment.id
                imageID.dispatchEvent(new Event('change'))

                imageSrc.style.display = 'block'
                delBtn.style.display = 'block'
                addBtn.style.display = 'none'
            })
            customUploader.open()
        }
    }

    removeImage(e) {
        e.preventDefault()

        getSiblings(e.target).map((el) => {
            if (el.classList.contains('rpr-ins-image-del')) {
                el.classList.add('rpr-hidden')
            }
            if (el.classList.contains('rpr-ins-image-set')) {
                el.style.display = 'block'
            }
            if (el.classList.contains('rpr_recipe_instructions_thumbnail')) {
                el.style.display = 'none'
                el.setAttribute('src', '')
            }
            if (el.classList.contains('rpr_recipe_instructions_image')) {
                el.value = ''
            }
        })
    }

    switchToBulkImporter(e = null) {
        e && e.preventDefault()

        this.instructionsImporterValue = ! this.instructionsImporterValue

        // Find the .rpr-ins-row and hide it
        document.querySelectorAll('.rpr-ins-row').forEach((el) => {
            el.hidden = ! this.instructionsImporterValue
        })

        // Find the .instruction-group and show it
        document.querySelectorAll('.instruction-group').forEach((el) => {
            el.hidden = ! this.instructionsImporterValue
        })

        // Find the .rpr-ins-bulk-importer-row and show it
        if (this.hasBulkInstructionImportButtonTarget) {
            document.querySelector('.rpr-ins-bulk-import').hidden = this.instructionsImporterValue
        }

        // Hide the "Add a instruction" and "Add a group" buttons
        if (this.hasAddInstructionRowButtonTarget) {
            if (! this.instructionsImporterValue) {
                this.addInstructionRowButtonTarget.style.display = 'none'
            } else {
                this.addInstructionRowButtonTarget.style.display = 'inline-flex'
            }
        }
        if (this.hasAddInstructionGroupButtonTarget) {
            if (! this.instructionsImporterValue) {
                this.addInstructionGroupButtonTarget.style.display = 'none'
            } else {
                this.addInstructionGroupButtonTarget.style.display = 'inline-flex'
            }
        }
    }

    stripUnwantedCharacters(e) {
        e.preventDefault()

        let text = e.clipboardData.getData('text')

        // Strip bullet characters and 1. 2) etc.
        text = text.replace(/[\u2022\u00B7\u00A7\u25CF]/g, '')
        text = text.replace(/(\d+)(\.|\))(\t)/g, '')

        // Get the current cursor position
        const selectionStart = e.target.selectionStart
        const selectionEnd = e.target.selectionEnd

        // Insert the stripped text at the cursor position
        const currentValue = e.target.value
        const newValue = currentValue.substring(0, selectionStart) + text + currentValue.substring(selectionEnd)
        e.target.value = newValue

        // Move the cursor to the end of the inserted text
        const newCursorPosition = selectionStart + text.length
        e.target.setSelectionRange(newCursorPosition, newCursorPosition)

        autosize.update(e.target)
    }

}
