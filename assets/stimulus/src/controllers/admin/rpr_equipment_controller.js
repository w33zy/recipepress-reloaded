import {Controller} from 'stimulus'
import {getSiblings, attachJQSortable} from '../../functions/functions'

export default class extends Controller {

    /** Keeps track of the button that was clicked */
    button = null

    connect() {
        window.rpr = window.rpr || {}
        rpr.rprEquipmentController = this
        attachJQSortable(this, '#recipe-equipment tbody')
    }

    /**
     * Add a new empty row to the recipe equipment table
     */
    duplicateRow(e) {
        e.preventDefault()

        // Get the last for that is empty of has (saved) information
        let lastRow = document.querySelectorAll('#recipe-equipment tbody tr:last-child').item(0)
        let lastEquip = document.querySelectorAll('#recipe-equipment tr.rpr-equip-row:last-child').item(0)

        // If a button was press grab the entire row it was pressed on
        if (e.target.classList.contains('rpr-equip-add-row') || e.code === 'Tab') {
            lastRow   = e.target.parentElement.parentElement
            lastEquip = lastRow
        }

        // Clone the row and its information
        let clonedEquip = lastEquip.cloneNode(true)

        // Set the placeholder and values on out inputs to be empty
        clonedEquip.querySelectorAll('input').forEach(function (item) {
            item.value = ''
        })

        // Hide the delete button on clone
        clonedEquip.querySelector('.rpr-equip-del-link').classList.add('rpr-hidden')

        // Insert the clone row after the row that clicked the button
        lastEquip.parentNode.insertBefore(clonedEquip, lastEquip.nextSibling)

        // Set the focus to the 'name' input field
        clonedEquip.querySelector('.rpr-equip-name-input').focus()

        // Reindex our rows
        this.updateIndex()
    }

    /**
     * Delete the current row
     *
     * @param {MouseEvent} e The click event
     */
    deleteRow(e) {
        e.preventDefault()

        const rows = getSiblings(e.target.parentElement.parentElement)
        const row = e.target.parentElement.parentElement
        const newRows = []
        let removedIndex = 0

        for (const [i, el] of rows.entries()) {
            if (el.classList.contains(row.className.split(' ').pop())) {
                removedIndex = i
                if (rows.length < 2) return // Prevent removing all rows
                el.remove()
            } else {
                newRows.push(el)
            }
        }

        if (newRows[removedIndex]) {
            newRows[removedIndex].querySelector('.rpr-equip-name-input').focus()
        }

        this.updateIndex()
    }

    /**
     * Duplicate the row if we are in the "notes" input and press
     * the `Tab` key
     *
     * @param {KeyboardEvent} e The click event
     */
    tabToDuplicate(e) {
        if (e.code === 'Tab' || e.key === 'Tab') {
            this.duplicateRow(e)
        }
    }

    /**
     * Each time we remove, add or drag & drop a new row,
     * we are going to recalculate the index of each row
     */
    updateIndex() {
        const rows = Array.from(document.querySelectorAll('#recipe-equipment tbody tr'))
            .filter(el => el.classList.contains('rpr-equip-row'))

        for (let i = 0; i < rows.length; i++) {
            let item = rows[i]

            // Update the `class` attribute
            item.classList.remove(item.className.split(' ').pop())
            item.classList.add('row-' + (i + 1))

            item.querySelectorAll('input').forEach((input) => {
                let name = input.attributes['name'].value
                name = name.replace(/\[(\d+)\]/, '[' + (i + 1) + ']')

                let id = input.attributes['id'].value
                id = id.replace(/\_(\d+)/, '_' + (i + 1))

                // Update the `name` attribute of the input
                input.setAttribute('name', name)

                // Update the `id` attribute of the input
                input.setAttribute('id', id)
            })
        }
    }

    addLink(e) {
        e.preventDefault()

        const data = {}
        this.button = e.target

        getSiblings(e.target).map((el) => {
            if (el.classList.contains('rpr_recipe_equipment_link')) {
                data.linkUrl = el.value
            }
            if (el.classList.contains('rpr_recipe_equipment_target')) {
                data.linkBehavior = el.value
            }
            data.linkText = ''
        })

        // Send info on which button was pressed as an event, to launch the add link modal
        this.dispatch('open', {detail: {button: e.target, ...data}})
    }

    removeLink(e) {
        e.preventDefault()

        getSiblings(e.target).map((el) => {
            if (el.classList.contains('rpr_recipe_equipment_link')) {
                el.value = ''
            }
            if (el.classList.contains('rpr_recipe_equipment_target')) {
                el.value = ''
            }
            if (el.classList.contains('rpr-equip-del-link')) {
                el.classList.add('rpr-hidden')
            }
            if (el.classList.contains('rpr-equip-add-link')) {
                el.title = ''
                el.classList.remove('has-link')
            }
        })
    }

    /**
     * Receives a CustomEvent with link details from
     * the link modal
     *
     * @param {CustomEvent} e The click event
     */
    getLinkData(e) {
        // If we didn't click a button in the controller, stop here
        if (!this.button) return

        this.setInputs(this.button, e)
    }

    /**
     * Set the hidden inputs
     *
     * @param {NodeList} list The click event
     * @param {CustomEvent} data The click event
     */
    setInputs(list, data) {
        const {linkUrl, linkBehavior} = data.detail
        //console.log(list, data)
        getSiblings(list).map((el) => {
            if (el.classList.contains('rpr_recipe_equipment_link')) {
                el.value = linkUrl
            }
            if (el.classList.contains('rpr_recipe_equipment_target')) {
                el.value = linkBehavior
            }
            if (el.classList.contains('rpr-equip-del-link')) {
                el.classList.remove('rpr-hidden')
            }
            if (el.classList.contains('rpr-equip-add-link')) {
                el.title = linkUrl
            }
        })
    }


}
