import { Application } from 'stimulus'
import controllers from 'stimulus-controllers'

const application = Application.start()
application.load(controllers);