import resolve from 'rollup-plugin-node-resolve'
import stimulus from 'rollup-plugin-stimulus'
import commonjs from '@rollup/plugin-commonjs'
// import copy from 'rollup-plugin-copy'
import { terser } from 'rollup-plugin-terser'

const production = !process.env.ROLLUP_WATCH

export default [
    {
        input: 'src/admin.js',
        output: {
            file: '../admin/js/rpr-admin-controllers.js',
            format: 'iife',
            sourcemap: !production,
        },
        context: 'window',
        plugins: [
            stimulus({
                // the directory your controllers are stored in
                basePath: './src/controllers/admin',

                // the name that is used to import Stimulus controllers in the app
                importName: 'stimulus-controllers',

                // whether or not to show 'this is undefined' warnings when importing @stimulus modules
                showWarnings: false,
            }),
            resolve(),
            commonjs({
                include: 'node_modules/**'
            }),
            production && terser(),
        ],
    },

    {
        input: 'src/frontend.js',
        output: {
            file: '../public/js/rpr-frontend-controllers.js',
            format: 'iife',
            sourcemap: !production,
        },
        context: 'window',
        plugins: [
            stimulus({
                // the directory your controllers are stored in
                basePath: './src/controllers/frontend',

                // the name that is used to import Stimulus controllers in the app
                importName: 'stimulus-controllers',

                // whether or not to show 'this is undefined' warnings when importing @stimulus modules
                showWarnings: false,
            }),
            resolve(),
            commonjs({
                include: 'node_modules/**'
            }),
            production && terser()
        ],
    },

];