* ~~Add the color picker library to admin pages~~
* ~~Add anchor tags to Ingredients, Instructions and Notes~~
* Add a thumbnail image upload field for each custom taxonomy
* ~~Look into add a user favorite recipes extension. Look at https://pippinsplugins.com/series/creating-a-user-follow-system-plugin.~~
* Implement the carousel JSON-LD markup as an extension. See https://search.google.com/structured-data/testing-tool.
* Add a per recipe option to the CTA extension. ~~Maybe add color settings.~~
* ~~Add sorting by rating in admin post column~~
* ~~Adjust the positioning of elements in the recipe editor on mobile~~
* Clicking star rating smooth scrolls to comment rating form
* ~~If an ingredient is deleted if causes a `Trying to get property 'name' of non-object` PHP notice on recipes still using the ingredient~~
* The settings page `std` feature no longer works because an empty string is now a valid setting
* Save custom metaboxes content to auto-saved revisions
* ~~Print function not recognizing the `.no-print` class~~
* ~~Create special functions to handle `data-` tags for ingredient unit and quantity~~
* ~~JSON-LD tag is not being added to `<head>` tag~~
* ~~Change the 2Column template to use CSS Grid~~
* ~~Add a "Jump to Recipe" feature~~
* ~~Replace "tags" with "keywords"~~
* ~~Label on settings page do not work~~
* ~~show the current link added to an ingredient, if present~~
* ~~the link target option in ingredients link modal is not working~~
* ~~Add "Recipes" column to the admin users table~~
* ~~A widget displaying the latest recipe ratings submitted (approved)~~
* A widget displaying the top recipes by ratings average or total ratings
* ~~The use "ingredient in listing" setting is not working~~
* ~~Complete the "recent recipes" widget~~
* Add video to recipe instructions. See http://www.javascriptkit.com/dhtmltutors/youtube-api-lightbox2.shtml 
https://developers.google.com/youtube/iframe_api_reference 
https://www.labnol.org/internet/light-youtube-embeds/27941/.
* ~~Add the plugin version to static asset files to be assist w/ cache flushing on new updates~~
* Add MediaVine jump to recipe integration
  `<div class="mv_slot_target" data-slot="recipe" data-render-default="true"></div>`
* ~~Integrate (nest) our JSON-LD output with Yoast SEO~~
* Add an option to make external link use the [sponsored](https://developers.google.com/search/docs/advanced/guidelines/qualify-outbound-links) attribute
* ~~The `rpr_add_key_to_recipe` task runs on new installs~~
* Add `<div class="mv_slot_target" data-slot="recipe" data-render-default="true"></div>`


https://johnblackbourn.com/post-meta-revisions-wordpress/
http://carolandrews.co.uk/saving-revisions-for-custom-post-type-meta-data/
https://core.trac.wordpress.org/ticket/20564
https://github.com/crowdfavorite/wp-revision-manager
